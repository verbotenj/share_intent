//
//  ShareViewController.swift
//  Share Extension
//
//  Created by Ales Verbic on 14/09/2020.
//


import UIKit
import Social


class ShareViewController: SLComposeServiceViewController {

    override func viewDidLoad() {
        print("viewDidload")
    }

    override func isContentValid() -> Bool {
        // Do validation of contentText and/or NSExtensionContext attachments here
        print("isContentValid")
        return true
    }

    override func didSelectPost() {
        // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
        print("didSelectPost")
        // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
//        self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
    }

    override func configurationItems() -> [Any]! {
        // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
        print("configurationItems")
        return []
    }

}
